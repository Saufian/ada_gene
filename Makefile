# Gene project makefile

EXEC_NAME=main

SOURCE_DIR=src
OBJECT_DIR=obj
EXEC_DIR=bin

GPR_FILE=gene.gpr
EXEC=$(EXEC_DIR)/$(EXEC_NAME)

.PHONY: clean mrproper

all: $(EXEC)

$(EXEC): $(SOURCE_DIR)/**
	gprbuild $(GPR_FILE)

clean:
	gprclean -q -c -P $(GPR_FILE)

mrproper: clean
	rm -f $(EXEC_DIR)/*