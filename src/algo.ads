package Algo is
  subtype Rate_Type is Integer;
  type Elem_Type is abstract tagged null record;

  --procedure Generate(Elem : in out Elem_Type) is abstract;
  procedure Generate (Elem : out Elem_Type) is abstract;
  procedure Combine(Elem1 : in Elem_Type; Elem2 : in Elem_Type; Out_Elem : out Elem_Type) is abstract;
  procedure Mute(Elem : in out Elem_Type) is abstract;
  function Rate(Elem : in Elem_Type) return Rate_Type is abstract;
  procedure Show_Elem(Elem : in Elem_Type) is abstract;
end Algo;