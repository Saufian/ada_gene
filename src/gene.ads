with Algo;

package Gene is
-- different ending condition (loop X time / find at least X as the best result / take X time / wait for the user interruption)
  type End_Condition_Type is (Loop_Limit, Result_Limit, Time_Limit, User_Interruption_Limit);
  type End_Condition (End_Type : End_Condition_Type) is record
    case End_Type is
      when Loop_Limit => Max_Iter : Positive;
      when Result_Limit => Result : Algo.Rate_Type;
      when Time_Limit => Time : Positive;
      when others => null;
      end case;
  end record;

-- probabilistic combination parameters
  subtype Percent_Type is Float range 0.0 .. 1.0;

-- different rate method (take the better / take the best in a random sub-list / ...)
  type Rate_Method_Type is (Global_Best, Best_From_Sample, Random_Sample);
  type Rate_Method (Rate_Type : Rate_Method_Type) is record
    case Rate_Type is
      when Best_From_Sample => Sample_Size : Positive;
      when Random_Sample => Selection_Probability : Percent_Type;
      when others => null;
    end case;
  end record;

-- use (or not) threading for rating (and so calculation)
  type Threading_Type is (No_Threading, As_We_Go_Threading);
  type Threading (Thread_Type : Threading_Type) is record
    case Thread_Type is
      when As_We_Go_Threading => Thread_Number : Positive;
      when others => null;
    end case;
  end record;

-- different selection system before combination.
  type Combination_Type is (Random, Best_With_Worst, Pairs_Of_Best);

  type Combination_Params is record
    Combination_Rules : Combination_Type := Pairs_Of_Best;
    Mix_Rate : Percent_Type := 0.33;  -- Percentage of new child that will be generated at each combination phase and will replace the worst elements
    Mutation_Rate : Percent_Type := 0.01;  -- Probability to call the mutate method for a new child
  end record;

-- Types for generic
  subtype List_Size_Type is Positive range 2 .. Positive'Last;

  generic
    type Elem is new Algo.Elem_Type with private;  -- Package implementing the Algo interface
    List_Size : List_Size_Type;  -- Maximum number of elements that will be used
  package Resolver is
    subtype List_Index_Type is Positive range 1..List_Size; 
    type Elem_List is array (List_Index_Type) of aliased Elem;


    procedure Start (End_Limit : End_Condition;
                     Rate_Procedure : Rate_Method := Rate_Method'(Rate_Type => Global_Best);
                     Thread_System : Threading := Threading'(Thread_Type => No_Threading);
                     Combination_Rules : Combination_Params := Combination_Params'(others => <>));


  private
    type Notation_Array_Type is array (List_Index_Type) of Algo.Rate_Type;

    procedure Single_Thread_Rating (List : in Elem_List; Notation : out Notation_Array_Type);
    procedure Multi_Thread_Rating (List : in Elem_List; Notation : out Notation_Array_Type; Thread_Number : in Positive);

    procedure Print_List (List : Elem_List; Notation : Notation_Array_Type);
  end Resolver;
end Gene;