-- Copyright (c) 2023 Thomas Bailleux
--
-- Licensed under the MIT license: https://opensource.org/licenses/MIT
-- Permission is granted to use, copy, modify, and redistribute the work.
-- Full license information available in the project LICENSE file.

generic
  type Task_Input_T  is private;
  type Task_Output_T is private;
  type Array_Range_T is (<>);
  with function Task_Function (Element : in Task_Input_T) return Task_Output_T;  -- TODO: restrict to protected ?
package Tasking_Manager is
  type Task_Input_Array_T is array (Array_Range_T) of aliased Task_Input_T;
  type Task_Output_Array_T is array (Array_Range_T) of Task_Output_T;

  procedure Launch_Task (Data_In : access Task_Input_Array_T; Thread_Number : in Positive);
  procedure Get_Task_Result (Data_Out : out Task_Output_Array_T);

  private
  type Rate_Array_Type is record
    Pos : Array_Range_T;
    Input_P : access Task_Input_T;
    Rate_Output : Task_Output_T;
  end record;
  Null_Rate_Array : constant Rate_Array_Type := (Array_Range_T'First, null, Rate_Output => <>);
  
  protected type Thread_Manager_Type is
    procedure Setup_Manager (Data_In : access Task_Input_Array_T);
    entry Begin_Work (Rate_Elem : out Rate_Array_Type; Continue : out Boolean);
    entry Next_Work  (Rate_Elem : in out Rate_Array_Type; Continue : out Boolean);
    entry Ask_Result (Data_Out : out Task_Output_Array_T);
  private
    procedure Get_Next (Rate_Elem : out Rate_Array_Type; Continue : out Boolean);
    Content_Array : access Task_Input_Array_T;
    Result_Array : Task_Output_Array_T;
    Array_Cursor : Array_Range_T := Array_Range_T'First;
    Is_Initialized : Boolean := False;  -- shows that the element is properly initialized
    Is_Content_Exhausted : Boolean := False;  -- indicates that there is still work to be done
    Is_Free : Boolean := False;  -- Manage operation concurrency
    Worker_Number : Natural := 0;  -- Track the number of working worker 
  end Thread_Manager_Type;

  Thread_Manager : aliased Thread_Manager_Type;

  task type Thread_Worker (Manager : access Thread_Manager_Type) is
    entry Start_Working;
  end Thread_Worker;
end Tasking_Manager;