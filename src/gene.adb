with Ada.Text_IO;  -- TODO : remove later
with Tasking_Manager;

package body Gene is
  
  package body Resolver is
    procedure Start (End_Limit : End_Condition;
                     Rate_Procedure : Rate_Method := Rate_Method'(Rate_Type => Global_Best);
                     Thread_System : Threading := Threading'(Thread_Type => No_Threading);
                     Combination_Rules : Combination_Params := Combination_Params'(others => <>)) is
      List : aliased Elem_List;
      Notation : Notation_Array_Type;
    begin
      -- Initialize Elem Array
      for Elem of List loop
        Elem.Generate;
      end loop;

      -- Rating
      case Thread_System.Thread_Type is
      when No_Threading =>
        Single_Thread_Rating(List, Notation);
      when As_We_Go_Threading =>
        if Thread_System.Thread_Number = 1 then
          Single_Thread_Rating(List, Notation);
        else
          Multi_Thread_Rating(List, Notation, Thread_System.Thread_Number);
        end if;
      end case;
        
      -- Reorganize List  -- TODO
      case Rate_Procedure.Rate_Type is
        when Global_Best => null;
        when Best_From_Sample => null;
        when Random_Sample => null;
      end case;

      -- Combining and mutating newly created element
      Combining : declare

      begin
        null;
      end Combining;

      -- End by printing the list as test
      Print_List (List, Notation);
    end Start;

    procedure Single_Thread_Rating (List : in Elem_List; Notation : out Notation_Array_Type) is
    begin
      for Element_Pos in List_Index_Type loop
        Notation(Element_Pos) := List(Element_Pos).Rate;
      end loop;
    end Single_Thread_Rating;

    procedure Multi_Thread_Rating (List : in Elem_List; Notation : out Notation_Array_Type; Thread_Number : in Positive) is
      package Manager is new Tasking_Manager (
        Task_Input_T => Elem,
        Task_Output_T => Algo.Rate_Type,
        Array_Range_T => (List_Index_Type),
        Task_Function => Rate);

      function Elem_To_Input (Elem_Array : in Elem_List)
          return Manager.Task_Input_Array_T is
        Input_Array : Manager.Task_Input_Array_T;
      begin
        for Index in Elem_Array'Range loop
          Input_Array(Index) := Elem_Array(Index);
        end loop;
        return Input_Array;
      end Elem_To_Input;

      function Output_To_Rate (Output_Array : in Manager.Task_Output_Array_T)
          return Notation_Array_Type is
        Rate_Array : Notation_Array_Type;
      begin
        for Index in Output_Array'Range loop
          Rate_Array(Index) := Output_Array(Index);
        end loop;
        return Rate_Array;
      end Output_To_Rate;

      Manager_Input  : aliased Manager.Task_Input_Array_T := Elem_To_Input(List);
      Manager_Output : Manager.Task_Output_Array_T;
    begin
      Manager.Launch_Task(Manager_Input'Access, Thread_Number);
      Manager.Get_Task_Result(Manager_Output);
      Notation := Output_To_Rate(Manager_Output);
    end Multi_Thread_Rating;
    
    procedure Print_List (List : Elem_List; Notation : Notation_Array_Type) is
    begin
      for Pos in List_Index_Type loop
        List(Pos).Show_Elem;
        Ada.Text_IO.Put_Line(" :" & Integer'Image(Integer(Notation(Pos))));
      end loop;
    end Print_List;
  end Resolver;
end Gene;