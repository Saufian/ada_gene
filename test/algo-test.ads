with Algo;

package Algo.Test is
  
  type Elem_Type is new Algo.Elem_Type with record
    Content : Integer;
  end record;
  
  procedure Generate (Elem : out Elem_Type);
  procedure Combine(Elem1 : in Elem_Type; Elem2 : in Elem_Type; Out_Elem : out Elem_Type);
  procedure Mute(Elem : in out Elem_Type);
  function Rate(Elem : in Elem_Type) return Algo.Rate_Type;
  procedure Show_Elem(Elem : in Elem_Type);

private
  procedure Assign(Elem1 : out Elem_Type; Elem2 : in Elem_Type);
  function CalculRate(Elem : in Elem_Type) return Algo.Rate_Type;
end Algo.test;