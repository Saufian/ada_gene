with Ada.Text_IO;
with Algo.Test;
with Gene;

procedure Test is
  package Gene_Test is new Gene.Resolver (Algo.Test.Elem_Type, 40);
begin
  Gene_Test.Start(End_Limit => (Gene.Loop_Limit, 10), Thread_System => (Gene.As_We_Go_Threading, 4));
end Test;