with Ada.Numerics.Discrete_Random;
with Ada.Text_IO;

package body Algo.Test is

  procedure Assign(Elem1 : out Elem_Type; Elem2 : in Elem_Type) is
  begin
    Elem1.Content := Elem2.Content;
  end Assign;

  procedure Generate(Elem : out Elem_Type) is
    subtype Content_Range is Natural range 0 .. 99;
    package Rand is new Ada.Numerics.Discrete_Random (Content_Range);
    G : Rand.Generator;
  begin
    Rand.Reset(G);
    Assign(Elem, Elem_Type'(Content => Rand.Random (G)));
  end Generate;

  procedure Combine(Elem1 : in Elem_Type; Elem2 : in Elem_Type; Out_Elem : out Elem_Type) is
  begin
    Assign(Out_Elem, Elem_Type'(Content => (Elem1.Content/10*10) + (Elem2.Content-Elem2.Content/10*10)));
  end Combine;

  procedure Mute(Elem : in out Elem_Type) is
  begin
    null;
  end Mute;

  function Rate(Elem : in Elem_Type) return Algo.Rate_Type is
  begin
    delay 1.0;  -- delay to imitate computing intensive operation
    return CalculRate(Elem);
  end Rate;

  function CalculRate(Elem : in Elem_Type) return Algo.Rate_Type is
    function To_Pos (test : in Integer) return Integer is
    begin
      return (if test in Positive then test else test * (-1));
    end To_Pos;
  begin
    return Algo.Rate_Type( Elem.Content * To_Pos(Elem.Content - 30) * To_Pos(Elem.Content - 99) / 2000); -- local : 14  / best : 74
  end CalculRate;

  procedure Show_Elem (Elem : in Elem_Type) is
  begin
    Ada.Text_IO.Put("Value" & Integer'Image(Elem.Content) & " (" & Integer'Image(Integer(CalculRate(Elem))) & ")");
  end Show_Elem;
end Algo.Test;