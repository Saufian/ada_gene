# Ada Gene

Ada Gene is a modular library providing multiple function to solve problems with
genetic algorithm.

This Tasking Manager will manage the execution of a function on each input
element, by distributing the load on several threads (number is set by the
user), then provide an array containing the results.

## Quick start

### Download

The source code can be download from
[Gitlab](https://gitlab.com/Saufian/ada_tasking_manager/-/archive/master/ada_gene-master.zip).

Additionnaly, you can clone this project:

```bash
git clone https://gitlab.com/Saufian/ada_gene.git
```

### Build

#### Build library

Building the project as a library does not work for now (will be done in a
future update).

You can still use this project by adding the files in `./src/` to your current
code base. You don't need to use any special compiler options to compile these
files.

#### Build example

Building the example is as simple as running the make command (with `gprbuild`
installed):

1. unpack the source code in a folder
2. enter into this folder
3. run the make command
4. launch the `test` executable in the `bin/` folder

```bash
# Compiling
make
# Executing
./bin/test
```

--

### Use the `Gene` library

--

## How does it work?

This project use the [Ada_Tasking_Manager](https://gitlab.com/Saufian/ada_tasking_manager) library to speed up processing.

--

## Roadmap

Future updates will focus on the following:

- Error raising
- Publication of library efficiency measures
- Fix library creation
- Add more inline documentation

## License

This project is under the MIT License (see the [LICENSE](LICENSE) file), which
means that you can do whatever you want with this project, as long as the
copyright notice and the permission notice remain. This project is provided
without warranty of any kind.

## Contact

You can contact the developer by opening an issue in the current repo.
